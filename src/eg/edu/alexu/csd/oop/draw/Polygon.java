package eg.edu.alexu.csd.oop.draw;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Shape;

public class Polygon extends Ishape{

	public void draw(Graphics canvas) {
		Graphics2D g=(Graphics2D) canvas;
		
		// TODO Auto-generated method stub
		canvas.setColor(this.getColor());
		java.awt.Polygon  a = new java.awt.Polygon() ;
		Double n=this.getProperties().get("npoints");
	
		for(int i=0;i<n;i++)
		{		
			Point b=new Point();
			String as="x";
			as+=(Integer.toString(i));
			Double a1=this.getProperties().get(as);
			as="y";
			as+=(Integer.toString(i));
			Double a2=this.getProperties().get(as);
			a.addPoint(a1.intValue(), a2.intValue());
			
		}
		g.draw(a);
		g.setColor(this.getFillColor());
		g.fill(a);
		return ;
	}
	
}