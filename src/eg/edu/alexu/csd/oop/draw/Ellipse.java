package eg.edu.alexu.csd.oop.draw;

import java.awt.Graphics;

public class Ellipse extends Ishape{

	public void draw(Graphics canvas) {
		// TODO Auto-generated method stub
		canvas.setColor(this.getColor());
		Double X=this.getProperties().get("x2");
		Double Y=this.getProperties().get("y2");
		int y= this.getPosition().y;
		int x= this.getPosition().x;
		int a=X.intValue();
		int b=Y.intValue();
		canvas.drawOval(Math.min(x, a), Math.min(y, b),Math.abs(x-a) ,Math.abs(y-b));
		canvas.setColor(this.getFillColor());
		canvas.fillOval(Math.min(x, a), Math.min(y, b),Math.abs(x-a) ,Math.abs(y-b));
		return ;
	}
	
}