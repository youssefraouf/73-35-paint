package eg.edu.alexu.csd.oop.draw;

import java.awt.Graphics;

public class Circle extends Ishape{

	public void draw(Graphics canvas) {
		// TODO Auto-generated method stub
		canvas.setColor(this.getColor());
		Double X=this.getProperties().get("radius");
		canvas.drawOval(this.getPosition().x, this.getPosition().y,X.intValue() ,X.intValue());
		canvas.setColor(this.getFillColor());
		canvas.fillOval(this.getPosition().x, this.getPosition().y, X.intValue(), X.intValue());
		return ;
	}
	
}