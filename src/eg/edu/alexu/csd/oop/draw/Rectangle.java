package eg.edu.alexu.csd.oop.draw;

import java.awt.Graphics;

public class Rectangle extends Ishape{

	public void draw(Graphics canvas) {
		// TODO Auto-generated method stub
		canvas.setColor(this.getColor());
		canvas.drawRect(this.getPosition().x, this.getPosition().y,Integer.parseInt( this.getProperties().get("width")+""),Integer.parseInt( this.getProperties().get("height")+""));
		canvas.setColor(this.getFillColor());
		canvas.drawRect(this.getPosition().x, this.getPosition().y,Integer.parseInt( this.getProperties().get("width")+""),Integer.parseInt( this.getProperties().get("height")+""));
		return ;
	}
	
}
