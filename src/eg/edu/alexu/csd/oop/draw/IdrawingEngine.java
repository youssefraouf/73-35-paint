package eg.edu.alexu.csd.oop.draw;

import java.awt.Color;
import java.awt.Graphics;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.List;
import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;
import org.xml.sax.*;
import org.w3c.dom.*;

import eg.edu.alexu.csd.oop.test.ReflectionHelper;

public class IdrawingEngine implements DrawingEngine {
	Shape[] shapes = new Shape[2000];
	Shape[] redoShapes = new Shape[20];
	Shape[] updateShapes = new Shape[20];
	int[] redoType = new int[20];
	int[] memory = new int[20];
	int memoryIndex = 0;
	int redoIndex = 0;
	int redoFlag = 0;
	int undoFlag = 0;
	int index = 0;

	@Override
	public void refresh(Graphics canvas) {
		// TODO Auto-generated method stub
		canvas.create();

	}

	public Class getPlugin(String classBinName) {
		try {

			// Create a new JavaClassLoader
			ClassLoader classLoader = this.getClass().getClassLoader();

			// Load the target class using its binary name
			Class loadedMyClass = classLoader.loadClass(classBinName);

			System.out.println("Loaded class name: " + loadedMyClass.getName());

			// Create a new instance from the loaded class
			Constructor constructor = loadedMyClass.getConstructor();
			Object myClassObject = constructor.newInstance();
			return loadedMyClass;

			// Getting the target method from the loaded class and invoke it using its name

		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

//   getlugin("eg.edu.alexu.csd.oop.draw");
	@Override
	public void addShape(Shape shape) {

		shapes[index] = shape;
		index++;
		System.out.println(shape.getColor());
		if (undoFlag == 0) {
			if (memoryIndex == 0 && redoFlag == 0) {
				redoIndex = 0;
			}
			if (redoFlag == 1) {
				redoFlag = 0;
			}
			if (memoryIndex == 20) {
				memoryIndex = 19;
				for (int i = 0; i < 19; i++) {
					memory[i] = memory[i + 1];
				}
				memoryIndex++;
			} else {
				memory[memoryIndex] = 0;
				memoryIndex++;
			}
		} else {
			undoFlag = 0;
		}
	}

	@Override
	public void removeShape(Shape shape) {
		// TODO Auto-generated method stub
		System.out.println("remove");
//		System.out.println(shape.getColor());
		for (int i = 0; i < index; i++) {
			if (shapes[i] == shape) {
				for (int j = i; j < index - 1; j++) {
					shapes[j] = shapes[j + 1];
				}
			}
		}
		index--;
		if (undoFlag == 0) {
			redoShapes[redoIndex] = shape;
			redoType[redoIndex] = 1;
			if (memoryIndex == 0 && redoFlag == 0) {
				redoIndex = 0;
			}
			if (redoFlag == 1) {
				redoFlag = 0;
			}
			if (memoryIndex == 20) {
				memoryIndex = 19;
				for (int i = 0; i < 19; i++) {
					memory[i] = memory[i + 1];
				}
				memoryIndex++;
			} else {
				memory[memoryIndex] = 1;
				memoryIndex++;
			}
		} else {
			undoFlag = 0;
		}
	}

	@Override
	public void updateShape(Shape oldShape, Shape newShape) {
		// TODO Auto-generated method stub
//		System.out.println("update");
		for (int i = 0; i < index; i++) {

			if (shapes[i] == oldShape) {
//				System.out.println(i);
				updateShapes[0] = oldShape;
				shapes[i] = newShape;

			}
		}
		if (undoFlag == 0) {
			if (memoryIndex == 0 && redoFlag == 0) {
				redoIndex = 0;
			}
			if (redoFlag == 1) {
				redoFlag = 0;
			}
			if (memoryIndex == 20) {
				memoryIndex = 19;
				for (int i = 0; i < 19; i++) {
					memory[i] = memory[i + 1];
				}
				memoryIndex++;
			} else {
				memory[memoryIndex] = 2;
				memoryIndex++;
			}
		} else {
			undoFlag = 0;
		}
	}

	@Override
	public Shape[] getShapes() {
		// TODO Auto-generated method stub
		Shape[] shapes1 = new Shape[index];
		for (int i = 0; i < index; i++) {
			shapes1[i] = shapes[i];
		}
		return shapes1;
	}

	@Override
	public List<Class<? extends Shape>> getSupportedShapes() {
		// TODO Auto-generated method stub
		Class youssef = getPlugin("eg.edu.alexu.csd.oop.draw.RoundRectangle");
		ReflectionHelper reflections = new ReflectionHelper();
		List<Class<? extends Shape>> list = new ArrayList<Class<? extends Shape>>();
		List<Class<?>> list1 = new ArrayList<Class<?>>();
		list1 = ReflectionHelper.findClassesImpmenenting(Shape.class, Shape.class.getPackage());
		for (int i = 0; i < list1.size(); i++) {
			list.add((Class<? extends Shape>) list1.get(i));
		}
		list.add(youssef);
		System.out.println(list);
		return list;
	}

	@Override
	public void undo() {
		// TODO Auto-generated method stub
		if (memoryIndex < 1) {
			return;
		}

		System.out.println("undo");
		undoFlag = 1;
		if (memory[memoryIndex - 1] == 0) {
			if (redoIndex == 20) {
				redoIndex = 19;
				for (int i = 0; i < 19; i++) {
					redoShapes[i] = redoShapes[i + 1];
					redoType[i] = redoType[i + 1];
				}
				redoIndex++;
			} else {

				redoShapes[redoIndex] = this.getShapes()[this.getShapes().length - 1];
				redoType[redoIndex] = 0;
				redoIndex++;
			}
//			
//			redoShapes[redoIndex]=this.getShapes()[this.getShapes().length - 1];
//			redoType[redoIndex]=0;
//			redoIndex++;

			removeShape(this.getShapes()[this.getShapes().length - 1]);
		} else if (memory[memoryIndex - 1] == 1) {
//			if (redoIndex == 20) {
//				redoIndex = 19;
//				for (int i = 0; i < 19; i++) {
//					redoShapes[i] = redoShapes[i + 1];
//					redoType[i] = redoType[i + 1];
//				}
//				redoIndex++;
//			} else {
//
//				redoShapes[redoIndex] = this.getShapes()[this.getShapes().length - 1];
//				redoType[redoIndex] = 1;
//				redoIndex++;
//			}
			addShape(redoShapes[redoIndex]);

		} else if (memory[memoryIndex - 1] == 2) {
			if (redoIndex == 20) {
				redoIndex = 19;
				for (int i = 0; i < 19; i++) {
					redoShapes[i] = redoShapes[i + 1];
					redoType[i] = redoType[i + 1];
				}
				redoIndex++;
			} else {

				redoShapes[redoIndex] = this.getShapes()[this.getShapes().length - 1];
				redoType[redoIndex] = 2;
				redoIndex++;
			}
			updateShape(this.getShapes()[0], updateShapes[0]);

		}
		memoryIndex--;
	}

	@Override
	public void redo() {
		// TODO Auto-generated method stub
		if (redoIndex < 1) {
			return;
		}
		redoFlag = 1;

		if (redoType[redoIndex - 1] == 0) {
			addShape(redoShapes[redoIndex - 1]);
		} else if (redoType[redoIndex - 1] == 1) {
			removeShape(redoShapes[redoIndex - 1]);
		} else if (redoType[redoIndex - 1] == 2) {
			updateShape(this.getShapes()[0], updateShapes[0]);
		}
		redoIndex--;
		System.out.println("redo");
	}

	@Override
	public void save(String path) {
		// TODO Auto-generated method stub
		DocumentBuilderFactory documentFactory = DocumentBuilderFactory.newInstance();

		DocumentBuilder documentBuilder = null;
		try {
			documentBuilder = documentFactory.newDocumentBuilder();
		} catch (ParserConfigurationException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		Document document = documentBuilder.newDocument();

		// root element
		Element root = document.createElement("drawEngine");
		document.appendChild(root);

		// employee element
		Shape[] shapes3 = getShapes();
		for (int i = 0; i < shapes3.length; i++) {
			Element shape = document.createElement("shape");

			root.appendChild(shape);
			// set an attribute to staff element
			Attr attr = document.createAttribute("id");
			attr.setValue(i + "");
			shape.setAttributeNode(attr);
//		Attr attr1 = document.createAttribute("colorOut");
//		System.out.println(i+""+shapes3[i]);
//		String colorOut = shapes3[i].getColor().toString();
//		attr1.setValue(colorOut);
//		shape.setAttributeNode(attr1);
//		Attr attr2 = document.createAttribute("colorFill");
//		String colorFill = shapes3[i].getFillColor().toString();
//		attr2.setValue(colorFill);
//		shape.setAttributeNode(attr2);

		}
		// create the xml file
		// transform the DOM Object to an XML File
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = null;
		try {
			transformer = transformerFactory.newTransformer();
		} catch (TransformerConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		DOMSource domSource = new DOMSource(document);
		StreamResult streamResult = new StreamResult(new File(path));

		// If you use
		// StreamResult result = new StreamResult(System.out);
		// the output will be pushed to the standard output ...
		// You can use that for debugging

		try {
			transformer.transform(domSource, streamResult);
		} catch (TransformerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		System.out.println("Done creating XML File");

	}

	@Override
	public void load(String path) {
		// TODO Auto-generated method stub
		Document dom;
		// Make an instance of the DocumentBuilderFactory
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		try {
			// use the factory to take an instance of the document builder
			DocumentBuilder db = dbf.newDocumentBuilder();
			// parse using the builder to get the DOM mapping of the
			// XML file
			dom = db.parse(path);

			Element doc = dom.getDocumentElement();
			memoryIndex = 0;
			redoIndex = 0;
			redoFlag = 0;
			undoFlag = 0;
			index = 0;
			Shape[] shapes1 = new Shape[2000];
			Shape[] redoShapes1 = new Shape[20];
			Shape[] updateShapes1 = new Shape[20];
			int[] redoType1 = new int[20];
			int[] memory1 = new int[20];
			shapes = shapes1;
			redoShapes = redoShapes1;
			updateShapes = updateShapes1;
			redoType = redoType1;
			memory = memory1;
			for (int i = 0; i < doc.getElementsByTagName("shape").getLength(); i++) {
				Shape shape = new Ishape();
//	            System.out.println(	doc.getElementsByTagName("shape").item(i).getAttributes().item(0).getNodeValue());
				Object Fillcolor = doc.getElementsByTagName("shape").item(i).getAttributes().item(0).getNodeValue();
				addShape(shape);
			}
			memoryIndex = 0;
			System.out.println(getShapes().length);

		} catch (ParserConfigurationException pce) {
			System.out.println(pce.getMessage());
		} catch (SAXException se) {
			System.out.println(se.getMessage());
		} catch (IOException ioe) {
			System.err.println(ioe.getMessage());
		}

//	        return false;
	}
}
