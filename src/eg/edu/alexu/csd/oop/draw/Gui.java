package eg.edu.alexu.csd.oop.draw;
import eg.edu.alexu.csd.oop.draw.Ishape;
import java.beans.*;
import java.io.*;
import java.io.IOException;
import javax.swing.filechooser.FileFilter;
import java.awt.BorderLayout;
import java.awt.Canvas;
import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.EventQueue;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.LayoutManager;
import java.awt.Panel;
import java.awt.Point;
import java.awt.Polygon;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.Window;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionAdapter;
import java.awt.event.MouseMotionListener;
import java.awt.geom.AffineTransform;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.geom.PathIterator;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.awt.geom.RectangularShape;
import java.awt.geom.RoundRectangle2D;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import javax.swing.JComponent;
import javax.swing.JFrame;
import java.awt.FlowLayout;
import javax.swing.SpringLayout;
import javax.swing.JButton;
import javax.swing.JColorChooser;

import java.awt.event.ActionListener;
import java.awt.event.ComponentListener;
import java.awt.event.ActionEvent;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JPanel;
import javax.swing.JInternalFrame;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;
import java.awt.SystemColor;
import java.awt.TextField;

import javax.swing.JScrollPane;
import javax.swing.JToolBar;
import javax.swing.JDesktopPane;
import javax.swing.JScrollBar;
import java.awt.Choice;
import java.awt.Button;
import java.awt.ScrollPane;
import javax.swing.colorchooser.ColorChooserComponentFactory;
import javax.swing.filechooser.FileFilter;
import javax.swing.JTextField;
import javax.swing.JFileChooser;
import javax.swing.JLabel;

public class Gui {

	private JFrame frame;
	static JInternalFrame internalFrame = new JInternalFrame("Paint Panel");
	public static ArrayList<Shape> shapes = new ArrayList();
	
	public static int x = 0, y = 0, X = -123, Y = -2123;
	public static ArrayList<Color> color = new ArrayList<Color>();
	public static Map<Integer, Color> Colorof = new HashMap<Integer, Color>();
	public static int flagshape = 0;
	public static java.awt.Shape b;
	public static Map<Integer, Integer> typeof = new HashMap<Integer, Integer>();
	public static Color c = Color.DARK_GRAY;
	public static Color fill = null;
	public static Map<Integer, Color> fillof = new HashMap<Integer, Color>();
	public static ArrayList[] states = new ArrayList[2000];
	public static int n = 0;
	public static int f;
	public static boolean in=false;
	public static Map<Integer, Map<Integer, Color>> Colorstates = new HashMap<Integer, Map<Integer, Color>>();
	// public static Map<Integer, Integer> typestates[] = new HashMap<Integer,
	// Integer>();
	public static Map<Integer, Map<Integer, Color>> fillstates = new HashMap<Integer, Map<Integer, Color>>();
	public static Vector<Polygon> polygonV=new Vector<Polygon>();
	public static Polygon polygon=new Polygon();
	static int p=0;
	static Vector<Point> p1=new Vector<Point>();
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Gui window = new Gui();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Gui() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */

	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 1123, 649);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		// frame.getContentPane().setLayout(null);
		frame.getContentPane().add(new Drawstuff());

		JPanel panel = new JPanel();
		panel.setBounds(7, 7, 1095, 1);

		panel.add(new Drawstuff());
		panel.getGraphics();
		panel.addMouseListener(m);
		panel.addMouseMotionListener(m2);
		frame.getContentPane().setLayout(null);
		frame.getContentPane().add(panel);
		panel.setLayout(null);

		
		internalFrame.setBounds(32, 52, 869, 533);
		internalFrame.getContentPane().add(new Drawstuff());
		internalFrame.getGraphics();
		
		internalFrame.addMouseListener(m);
		internalFrame.addMouseMotionListener(m2);
		internalFrame.setCursor(cursor);
		frame.getContentPane().add(internalFrame);

		JButton btnChooseColor = new JButton("Choose Color");
		btnChooseColor.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				c = new JColorChooser().showDialog(null, "Please pick your color", c);
			}
		});
		btnChooseColor.setBounds(957, 113, 85, 21);
		frame.getContentPane().add(btnChooseColor);

		JButton btnRectangle = new JButton("Rectangle");
		btnRectangle.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				flagshape = 1;
			}
		});
		btnRectangle.setBounds(32, 21, 85, 21);
		frame.getContentPane().add(btnRectangle);

		JButton btnLine = new JButton("line");
		btnLine.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				flagshape = 3;
			}
		});
		btnLine.setBounds(150, 21, 85, 21);
		frame.getContentPane().add(btnLine);

		JButton btnEllipse = new JButton("Ellipse");
		btnEllipse.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				flagshape = 4;
			}
		});
		btnEllipse.setBounds(268, 21, 85, 21);
		frame.getContentPane().add(btnEllipse);

		JButton btnFillColor = new JButton("fill Color");
		btnFillColor.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				fill = new JColorChooser().showDialog(null, "Please pick your color", c);
				flagshape = -1;

			}
		});
		btnFillColor.setBounds(957, 174, 85, 21);
		frame.getContentPane().add(btnFillColor);

		JButton btnNewButton = new JButton("Undo");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (n == 0)
					return;

				if (n == 1) {
					shapes.clear();
				}
				n--;
				shapes = (ArrayList) states[n].clone();
				// fillof.clear();
				Map<Integer, Color> a = new HashMap<Integer, Color>();
				a = fillstates.get(n);
				fillof = a;
			//	Colorof=Colorstates.get(n);
				System.out.println(shapes);
				System.out.println(n);
			}
		});
		btnNewButton.setBounds(924, 249, 85, 21);
		frame.getContentPane().add(btnNewButton);

		JButton btnNewButton_1 = new JButton("Redo");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				n++;
				if (n > f) {
					n = f;
					return;
				}
				shapes = (ArrayList) states[n].clone();
				System.out.println(n);
				Map<Integer, Color> a = new HashMap<Integer, Color>();
				a = fillstates.get(n);
				fillof = a;
			//	Colorof=Colorstates.get(n);
				System.out.println(n + "  -a7as  ");

				System.out.println(fillstates);
			}
		});
		btnNewButton_1.setBounds(1019, 249, 85, 21);
		frame.getContentPane().add(btnNewButton_1);

		JButton btnMove = new JButton("Move");
		btnMove.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				flagshape = -2;

			}
		});
		btnMove.setBounds(957, 317, 85, 21);
		frame.getContentPane().add(btnMove);
		 cursor=  Cursor.getPredefinedCursor(Cursor.SW_RESIZE_CURSOR);
		internalFrame.setCursor(cursor);
		
		JButton btnPolygon = new JButton("polygon");
		btnPolygon.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				flagshape=5;
			}
		});
		btnPolygon.setBounds(957, 21, 85, 21);
		frame.getContentPane().add(btnPolygon);
		
		textField = new JTextField();
		textField.setBounds(946, 49, 96, 19);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		
		JButton btnSaveAs = new JButton("Save as");
		btnSaveAs.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			JFileChooser chooser=new JFileChooser();
			chooser.setFileFilter(new FileTypeFilter(".xml","XML File"));
			int result=chooser.showSaveDialog(null);
			if(result==JFileChooser.APPROVE_OPTION)
			{
				FileOutputStream f;
				try {
					f = new FileOutputStream(chooser.getSelectedFile());
					XMLEncoder encoder =new XMLEncoder(f);
				
					encoder.writeObject(shapes);
					encoder.writeObject(color);
					encoder.writeObject(Colorof);
					encoder.writeObject(typeof);
					encoder.writeObject(fillof);
					//encoder.writeObject(g.getShapes());
					encoder.close();
					f.close();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					JOptionPane.showMessageDialog(null, e1.getMessage());
					e1.printStackTrace();
				}
				
			}
			
			
			}
		});
		btnSaveAs.setBounds(957, 459, 85, 21);
		frame.getContentPane().add(btnSaveAs);
		
		JButton btnLoad = new JButton("load");
		btnLoad.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFileChooser chooser=new JFileChooser();
				chooser.setFileFilter(new FileTypeFilter(".xml","XML File"));
				
				int result=chooser.showOpenDialog(null);
				if(result==JFileChooser.APPROVE_OPTION)
				{
					FileInputStream f;
					try {
						f = new FileInputStream(chooser.getSelectedFile());
						XMLDecoder decoder =new XMLDecoder(f);
						shapes=(ArrayList<Shape>) decoder.readObject();
						color=(ArrayList<Color>) decoder.readObject();
						Colorof=(Map<Integer, Color>) decoder.readObject();
						typeof=(Map<Integer, Integer>) decoder.readObject();
						fillof=(Map<Integer, Color>) decoder.readObject();
					
						decoder.close();
						f.close();
						//System.out.println(g.getShapes());
						
					//	System.out.println(g.shapesg);
						
						
						
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						JOptionPane.showMessageDialog(null, e1.getMessage());
						e1.printStackTrace();
					}
					
				}
				
			}
		});
		btnLoad.setBounds(957, 397, 85, 21);
		frame.getContentPane().add(btnLoad);
		
		JButton btnDelete = new JButton("delete");
		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				flagshape=-5;
			}
		});
		btnDelete.setBounds(957, 216, 85, 21);
		frame.getContentPane().add(btnDelete);
		
		JButton btnLoadRoundRec = new JButton("Load Round Rec");
		btnLoadRoundRec.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				flagshape=6;
				
			}
		});
		btnLoadRoundRec.setBounds(610, 21, 85, 21);
		frame.getContentPane().add(btnLoadRoundRec);
		
		textField_1 = new JTextField();
		textField_1.setBounds(699, 23, 55, 19);
		frame.getContentPane().add(textField_1);
		textField_1.setColumns(10);
		
		textField_2 = new JTextField();
		textField_2.setBounds(769, 23, 42, 19);
		frame.getContentPane().add(textField_2);
		textField_2.setColumns(10);
		
		JLabel lblArcwidth = new JLabel("arcwidth");
		lblArcwidth.setBounds(699, 7, 46, 13);
		frame.getContentPane().add(lblArcwidth);
		
		JLabel lblArclength = new JLabel("arcLength");
		lblArclength.setBounds(765, 7, 46, 13);
		frame.getContentPane().add(lblArclength);
		internalFrame.setVisible(true);
	}
	
	private class Drawstuff extends JComponent {
		public void paint(Graphics g) {

			Graphics2D g2 = (Graphics2D) g;

			g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		
			if(in)
			{
				for (int i = 0; i < shapes.size(); i++) {
					if(flagshape==-2&&index==i)
					{
						continue;
					}
					
					g2.setColor(Colorof.get(i));
					if (fillof.get(i) != null) {
						g2.setColor(fillof.get(i));
						g2.fill((Shape) shapes.get(i));
						g2.setColor(Colorof.get(i));

					}
					g2.draw((Shape) shapes.get(i));
					
				}
				
				repaint();
				
				return;
			}
			
			if (flagshape == 0 && x != X && y != Y) {

				b = new RoundRectangle2D.Double(X, Y, 30, 30, 30, 30);
				fillof.put(shapes.size(), c);
				Colorof.put(shapes.size(), c);
				typeof.put(shapes.size(), flagshape);
				shapes.add(b);
			} else if (flagshape == 1) {
				b = new Rectangle(Math.min(x, X), Math.min(y, Y), Math.abs(X - x), Math.abs(Y - y));
			} else if ( flagshape == 4) {
				b = new Ellipse2D.Double(Math.min(x, X), Math.min(y, Y), Math.abs(X - x), Math.abs(Y - y));
			} else if (flagshape == 3) {
				b =new Line2D.Double(x,y,X,Y);
				
			}
			
			else if(flagshape==5)
			{	
				b=polygon;
			}
			else if(flagshape==6)
			{
				double a1=Double.parseDouble(textField_1.getText());
				double a2=Double.parseDouble(textField_2.getText());
				b=new RoundRectangle2D.Double(Math.min(x, X), Math.min(y, Y), Math.abs(X - x), Math.abs(Y - y), a1, a2);
			}
			if (flagshape >=0) {
				
				g2.setColor(c);
				g2.draw(b);
			}
			
			if(flagshape==-2)
			{
				  index=-1;
				dim = new Rectangle2D.Double(1, 1, 1, 1) ;
				
				for(int i=0;i<shapes.size();i++)
				{
					if(((Shape)shapes.get(i)).contains(x, y))
					{
						index=i;
						dim=((Shape)shapes.get(i)).getBounds();
						break;
					}
				}
				if(index!=-1)
				{
					
				java.awt.Shape a;
				if(typeof.get(index)==1)
				 a=new Rectangle2D.Double(X-dim.getMinX(), Y-dim.getMinY(),dim.getWidth(),dim.getHeight());
				else if(typeof.get(index)==2||typeof.get(index)==4)
					a=new Ellipse2D.Double(X-dim.getMinX(), Y-dim.getMinY(),dim.getWidth(),dim.getHeight());
				else if(typeof.get(index)==3)
				{
					a=new Line2D.Double(X-dim.getMinX(), Y-dim.getMinY(),dim.getWidth(),dim.getHeight());
				}
				else if(typeof.get(index)==6)
				{
					Double a1=Double.parseDouble(textField_1.getText());
					Double a2=Double.parseDouble(textField_2.getText());
					 a=new RoundRectangle2D.Double(X-dim.getMinX(), Y-dim.getMinY(),dim.getWidth(),dim.getHeight(),a1,a2);
				}
				else if(typeof.get(index)==5)
				{
					
					Polygon h=(Polygon) shapes.get(index);
					if(p1.isEmpty())
					{		
				
					for(int i=0;i<h.npoints;i++)
					{
						p1.add(new Point(x-h.xpoints[i],y-h.ypoints[i]));
					}
				 }	
					for(int i=0;i<h.npoints;i++)
					{
						
						h.xpoints[i]=X-p1.get(i).x;
						h.ypoints[i]=Y-p1.get(i).y;
					}
					
					
					a=h;
				}
				else
				{
					a= new RoundRectangle2D.Double(X, Y, 30, 30, 30, 30);
				}
				//shapes.set(index, a);
				//g2.setColor(Colorof.get(index));
				if(fillof.get(index)!=null)
				{
				g2.setColor(fillof.get(index));
				g2.fill(a);
				}
				g2.setColor(Colorof.get(index));
				g2.draw(a);
				}
				//repaint();
			}
			
			for (int i = 0; i < shapes.size(); i++) {
				if(flagshape==-2&&index==i||shapes.get(i)==null)
				{
					continue;
				}
				
				g2.setColor(Colorof.get(i));
				if (fillof.get(i) != null) {
					g2.setColor(fillof.get(i));
					g2.fill((Shape) shapes.get(i));
					g2.setColor(Colorof.get(i));

				}
				g2.draw((Shape) shapes.get(i));
				// System.out.println(n);
			}

			repaint();
		}
	}
	static Cursor cursor = new Cursor(Cursor.HAND_CURSOR);
	
		static int index=0;
		static Rectangle2D dim;
		
		public static MouseListener m=new MouseAdapter(){

		@Override public void mouseReleased(MouseEvent e){
			
			
			if(in)
			{
				return;
			}
			n++;X=e.getX();Y=e.getY();
			f=n;
			if(flagshape==-5)
			{
				int ir=-12;
				for(int i=0;i<shapes.size();i++)
				{
					if(shapes.get(i).contains(X, Y))
					{
						ir=i;
						break;
					}
				}
				
				if(ir<0)
				{
					return;
				}
				Map<Integer,Color>c1 = new HashMap<Integer,Color>(),f1=new HashMap<Integer,Color>();
				for(int i=0;i<shapes.size();i++)
				{
					if(i==ir)
						continue;
					if(i<ir)
					{
						c1.put(i, Colorof.get(i));
						f1.put(i, fillof.get(i));
					}
					else
					{
						c1.put(i-1, Colorof.get(i));
						f1.put(i-1, fillof.get(i));
					}
					
				
				}
				shapes.remove(ir);
				Colorof=c1;
				fillof=f1;
				states[n]=(ArrayList) shapes.clone();
				Colorstates.put(n, Colorof);
				fillstates.put(n, fillof);
				return;
				
			}
			if(flagshape==-2)
			{
				  index=-1;
					dim = new Rectangle2D.Double(1, 1, 1, 1) ;
					
					for(int i=0;i<shapes.size();i++)
					{
						if(((Shape)shapes.get(i)).contains(x, y))
						{
							index=i;
							dim=((Shape)shapes.get(i)).getBounds();
							break;
						}
					}
					if(index==-1)
						return;
				java.awt.Shape a;
				if(typeof.get(index)==1)
					 a=new Rectangle2D.Double(X-dim.getMinX(), Y-dim.getMinY(),dim.getWidth(),dim.getHeight());
					else if(typeof.get(index)==2||typeof.get(index)==4)
						a=new Ellipse2D.Double(X-dim.getMinX(), Y-dim.getMinY(),dim.getWidth(),dim.getHeight());
					else if(typeof.get(index)==1)
					{
						a=new Line2D.Double(X-dim.getMinX(), Y-dim.getMinY(),dim.getWidth(),dim.getHeight());
					}
					else if(typeof.get(index)==6)
					{
						Double a1=Double.parseDouble(textField_1.getText());
						Double a2=Double.parseDouble(textField_2.getText());
						 a=new RoundRectangle2D.Double(X-dim.getMinX(), Y-dim.getMinY(),dim.getWidth(),dim.getHeight(),a1,a2);
					}
					else if(typeof.get(index)==5)
					{
						for(int i=0;i<shapes.size();i++)
						{
							if(((Shape)shapes.get(i)).contains(X, Y))
							{
								index=i;
								dim=((Shape)shapes.get(i)).getBounds();
								break;
							}
						}
						Polygon h=(Polygon) shapes.get(index);
						
						for(int i=0;i<h.npoints;i++)
						{
							h.xpoints[i]=X-p1.get(i).x;
							h.ypoints[i]=Y-p1.get(i).y;
						}
						p1.clear();
						//h.reset();
						a=h;
						
					}
					else
					{
						a= new RoundRectangle2D.Double(X, Y, 30, 30, 30, 30);
					}
				shapes.remove(index);
				shapes.add(index, a);
				states[n]=(ArrayList)shapes.clone();
				System.out.println(states[n]);
				fillstates.put(n, fillof);
				Colorstates.put(n,Colorof);
				index=-1;
				return ;
			}
			
		if(flagshape==-1)
		{Map<Integer,Color>a=new HashMap<Integer,Color>();for(int i=0;i<shapes.size();i++){if(((Shape)shapes.get(i)).contains(x,y)){fillof.put(i,fill);

		}a.put(i,fillof.get(i));}
		states[n]=(ArrayList)shapes.clone();fillstates.put(n,a);System.out.println("a9a "+n);System.out.println(fillof);System.out.println(fillstates.get(n));f=n;return;
		}
		
		b=new RoundRectangle2D.Double(X,Y,30,30,30,30);if(flagshape==0){

		b=new RoundRectangle2D.Double(X,Y,30,30,30,30);fillof.put(shapes.size(),c);

		}else if(flagshape==1){b=new Rectangle(Math.min(x,X),Math.min(y,Y),Math.abs(X-x),Math.abs(Y-y));}
		else if(flagshape==2||flagshape==4)
		{b=new Ellipse2D.Double(Math.min(x,X),Math.min(y,Y),Math.abs(X-x),Math.abs(Y-y));}
		else if(flagshape==3){b=new Line2D.Double(x,y,X,Y);}
		else if(flagshape==5)
		{
			String s=textField.getText();
			if(polygon.npoints==Integer.parseInt(s))
			{
				b=polygon;
			}
			else
			{
				return;
			}
		}
		else if(flagshape==6)
		{
			double a1=Double.parseDouble(textField_1.getText());
			double a2=Double.parseDouble(textField_2.getText());
			b=new RoundRectangle2D.Double(Math.min(x, X), Math.min(y, Y), Math.abs(X - x), Math.abs(Y - y), a1, a2);
		}
		fill=null;
		Colorof.put(shapes.size(),c);typeof.put(shapes.size(),flagshape);shapes.add(b);
		System.out.println(b);
		// n++;
		states[n]=(ArrayList)shapes.clone();
		Map<Integer,Color>a=new HashMap<Integer,Color>();
		for(int i=0;i<shapes.size();i++){
			if(((Shape)shapes.get(i)).contains(x,y))
			{fillof.put(i,fill);

		}
			Color k=fillof.get(i);a.put(i,k);
			}
		fillstates.put(n,a);
		Colorstates.put(n, Colorof);
		f=n;
		x=X;
		y=Y;
		
		}
		@Override public void mousePressed(MouseEvent e){
			
			x=X=e.getX();
			y=Y=e.getY();
			if(flagshape==5)
			{
				String s=textField.getText();
				if(polygon.npoints==Integer.parseInt(s))
				{
					polygon=new Polygon();
				}
			polygon.addPoint(X, Y);
			
			}
		}};
		 static Rectangle dim1 = null;
		 static int indexsize=0;
		static int t=0;
	public static MouseMotionListener m2 = new MouseMotionAdapter() {
		@Override public void mouseMoved(MouseEvent e)
		{
			int z=e.getX();
			int l=e.getY();
			JInternalFrame fra= internalFrame;
			 cursor=  Cursor.getPredefinedCursor(Cursor.HAND_CURSOR);
			 in=false;
			
		for(int i=0;i<shapes.size();i++)
		{
			dim1=shapes.get(i).getBounds();
			//System.out.println(dim1.getMaxX()+" "+(z));
			int z1=z-10;
			int z2=z+10;
			int l1=l-20;
			int l2=l+20;
			if(typeof.get(i)==2)
			{
				int m=(int)(dim1.getMaxX()-dim1.getMinX())/2;
				z1=z-10;
				z2=z+10;
				l1=l-10;
				l2=l+10;
			}
			if(z1<=dim1.getMaxX()&&z2>=dim1.getMaxX()&&l1<=dim1.getMaxY()&&l2>=dim1.getMaxY())
			{
				t=1;
				indexsize=i;	
				in =true;
				break;
			}
			else if(z1<=dim1.getMaxX()-20&&z2>=dim1.getMaxX()-20&&l1<=dim1.getMinY()+30&&l2>=dim1.getMinY()+30)
			{
				t=2;
				indexsize=i;	
				in =true;
				break;
			}
			else if(z1<=dim1.getMinX()&&z2>=dim1.getMinX()&&l1<=dim1.getMaxY()&&l2>=dim1.getMaxY())
			{
				t=3;
				indexsize=i;	
				in =true;
				break;
			}
			else if(z1<=dim1.getMinX()&&z2>=dim1.getMinX()&&l1<=dim1.getMinY()+30&&l2>=dim1.getMinY()+30)
			{
				t=4;
				indexsize=i;	
				in =true;
				break;
			}
		}
		
		if(in==false)
		{
			cursor=Cursor.getPredefinedCursor(Cursor.HAND_CURSOR);	
		}
		else
		{
			if(t==1)
			cursor=Cursor.getPredefinedCursor(Cursor.SE_RESIZE_CURSOR);
			else if(t==2)
			{
				cursor=Cursor.getPredefinedCursor(Cursor.NE_RESIZE_CURSOR);
			}
			else if(t==3)
			{
				cursor=Cursor.getPredefinedCursor(Cursor.SW_RESIZE_CURSOR);
			}
			else if(t==4)
			{
				cursor=Cursor.getPredefinedCursor(Cursor.NW_RESIZE_CURSOR);
			}
			
		}
			fra.setCursor(cursor);
	
		}

		@Override
		public void mouseDragged(MouseEvent e) {
			X = e.getX();
			Y = e.getY();
			if(!in)
				return;
			if(typeof.get(indexsize)==1)
			{
				double s=0;
				double h = 0;
				java.awt.Shape a=null;
				in=true;
				if(t==1)
				{
					s=dim1.getWidth()+X-dim1.getMaxX();
					 h=dim1.getHeight()+Y-dim1.getMaxY();
					 a=new Rectangle2D.Double(dim1.getMinX(),dim1.getMinY(),s, h);
				}
				else if(t==2)
				{
					s=dim1.getWidth()+X-dim1.getMaxX();
					h=dim1.getHeight()+dim1.getMinY()-Y;
					 a=new Rectangle2D.Double(dim1.getMinX(),Y,s, h); 
				}
				else if(t==3)
				{
					s=dim1.getWidth()+dim1.getMinX()-X;
					h=dim1.getHeight()+Y-dim1.getMaxY();
					 a=new Rectangle2D.Double(X,dim1.getMinY(),s, h); 
				}
				else if(t==4)
				{
					s=dim1.getWidth()+dim1.getMinX()-X;
					h=dim1.getHeight()+dim1.getMinY()-Y;
					a=new Rectangle2D.Double(X,Y,s, h); 
				}
				shapes.set(indexsize, a);
			}
			else if(typeof.get(indexsize)==4)
			{
				double s=0;
				double h = 0;
				java.awt.Shape a=null;
				in=true;
				if(t==1)
				{
					s=dim1.getWidth()+X-dim1.getMaxX();
					 h=dim1.getHeight()+Y-dim1.getMaxY();
					 a=new Ellipse2D.Double(dim1.getMinX(),dim1.getMinY(),s, h);
				}
				else if(t==2)
				{
					s=dim1.getWidth()+X-dim1.getMaxX();
					h=dim1.getHeight()+dim1.getMinY()-Y;
					 a=new Ellipse2D.Double(dim1.getMinX(),Y,s, h); 
				}
				else if(t==3)
				{
					s=dim1.getWidth()+dim1.getMinX()-X;
					h=dim1.getHeight()+Y-dim1.getMaxY();
					 a=new Ellipse2D.Double(X,dim1.getMinY(),s, h); 
				}
				else if(t==4)
				{
					s=dim1.getWidth()+dim1.getMinX()-X;
					h=dim1.getHeight()+dim1.getMinY()-Y;
					a=new Ellipse2D.Double(X,Y,s, h); 
				}
				shapes.set(indexsize, a);
			}
			else if(typeof.get(indexsize)==6)
			{
				double s=0;
				double h = 0;
				Rectangle2D d=shapes.get(indexsize).getBounds2D();
				
				java.awt.Shape a=null;
				in=true;
				double a1=Double.parseDouble(textField_1.getText());
				 double a2=Double.parseDouble(textField_2.getText());
				if(t==1)
				{
					s=dim1.getWidth()+X-dim1.getMaxX();
					 h=dim1.getHeight()+Y-dim1.getMaxY();
					 
					 a=new RoundRectangle2D.Double(dim1.getMinX(),dim1.getMinY(),s, h,a1,a2);
				}
				else if(t==2)
				{
					s=dim1.getWidth()+X-dim1.getMaxX();
					h=dim1.getHeight()+dim1.getMinY()-Y;
					 a=new RoundRectangle2D.Double(dim1.getMinX(),Y,s, h,a1,a2); 
				}
				else if(t==3)
				{
					s=dim1.getWidth()+dim1.getMinX()-X;
					h=dim1.getHeight()+Y-dim1.getMaxY();
					 a=new RoundRectangle2D.Double(X,dim1.getMinY(),s, h,a1,a2); 
				}
				else if(t==4)
				{
					s=dim1.getWidth()+dim1.getMinX()-X;
					h=dim1.getHeight()+dim1.getMinY()-Y;
					a=new RoundRectangle2D.Double(X,Y,s, h,a1,a2); 
				}
				shapes.set(indexsize, a);
			}
			
		
		}
	};
	
	public static eg.edu.alexu.csd.oop.draw.Rectangle setRec(int x,int y,int X,int Y)
	{
		eg.edu.alexu.csd.oop.draw.Rectangle  a=new eg.edu.alexu.csd.oop.draw.Rectangle ();
		 Map<String, java.lang.Double> properties=new HashMap<String,java.lang.Double>();
		 a.setPosition(new Point(x,y));
		 Double x2=Double.valueOf(X);
		 Double y2=Double.valueOf(Y);
		 properties.put("Width",java.lang.Double.valueOf(100));
		properties.put("Length",java.lang.Double.valueOf(100));
		properties.put("x2",x2);
		properties.put("y2", y2);
		a.setProperties(properties);
		return  a;
	}
	
	public static eg.edu.alexu.csd.oop.draw.Circle setCircle(int x,int y,int X,int Y)
	{
		eg.edu.alexu.csd.oop.draw.Circle  a=new eg.edu.alexu.csd.oop.draw.Circle ();
		 Map<String, java.lang.Double> properties=new HashMap<String,java.lang.Double>();
			a.setPosition(new Point(x,y));
			int as=(X-x)*(X-x)+(X-y)*(X-y);
			properties.put("radius",  java.lang.Double.valueOf(Math.sqrt(as)));
			a.setProperties(properties);
		return  a;
	}
	public static Line setLine(int x,int y,int X,int Y)
	{
		Line a=new Line();
		 Map<String, java.lang.Double> properties=new HashMap<String,java.lang.Double>();
		 a.setPosition(new Point(x,y));
		 Double x2=Double.valueOf(X);
		 Double y2=Double.valueOf(Y);
		properties.put("x2",x2);
		properties.put("y2", y2);
		a.setProperties(properties);
		return  a;
	}
	
	private static JTextField textField;
	private static JTextField textField_1;
	private static JTextField textField_2;
}